﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using RelayCommandRt;
using SQLite;
using SQLiteApp.Annotations;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace SQLiteApp
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private List<Note> _notes;

        public MainViewModel()
        {
            AddCommand = new RelayCommand(_ => OnAdd());
            Init();
        }

        private void OnAdd()
        {
            var frame = Window.Current.Content as Frame;
            frame.Navigate(typeof (AddPage));
        }

        private async void Init()
        {
            var db = new SQLiteAsyncConnection("notes.sqldb");
            await db.CreateTableAsync<Note>();
            Notes = await db.Table<Note>().ToListAsync();
        }

        public RelayCommand AddCommand { get; set; }

        public List<Note> Notes
        {
            get { return _notes; }
            set
            {
                if (Equals(value, _notes)) return;
                _notes = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}