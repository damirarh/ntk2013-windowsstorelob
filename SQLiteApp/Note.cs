﻿using SQLite;

namespace SQLiteApp
{
    public class Note
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Contents { get; set; }
    }
}