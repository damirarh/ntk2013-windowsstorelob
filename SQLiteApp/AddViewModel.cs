﻿using System.Threading.Tasks;
using RelayCommandRt;
using SQLite;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace SQLiteApp
{
    public class AddViewModel
    {
        public AddViewModel()
        {
            Note = new Note();
            AddCommand = new RelayCommand(async _ => await OnAdd());
        }

        private async Task OnAdd()
        {
            var db = new SQLiteAsyncConnection("notes.sqldb");
            await db.InsertAsync(Note);

            var frame = Window.Current.Content as Frame;
            frame.GoBack();
        }

        public Note Note { get; set; }
        public RelayCommand AddCommand { get; set; }
    }
}